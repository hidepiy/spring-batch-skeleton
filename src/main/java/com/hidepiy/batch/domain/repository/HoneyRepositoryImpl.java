package com.hidepiy.batch.domain.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import com.hidepiy.batch.domain.dto.Honey;

public class HoneyRepositoryImpl {
    private final Map<String, Honey> honeyMap = new ConcurrentHashMap<>();
    
    @PostConstruct
    public void loadDummyData() {
        honeyMap.put("honey001", new Honey("honey001"));
        honeyMap.put("honey002", new Honey("honey002"));
        honeyMap.put("honey003", new Honey("honey003"));
    }
    
    public Iterable<Honey> findAll() {
        return honeyMap.values();
    }

}
