package com.hidepiy.batch.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hidepiy.batch.domain.dto.Honey;

@Configuration
@EnableBatchProcessing
public class HoneyHuntJobConf {
    
    @Bean
    public Job honeyHuntJob(JobBuilderFactory jobBuilderFactory,
            Step honeyHuntStep,
            Step honeyMoonStep) {
         return jobBuilderFactory
                .get("honeyHuntJob")
                .incrementer(new RunIdIncrementer())
                .start(honeyHuntStep)
                .next(honeyMoonStep)
                .build();
    }
    
    @Bean
    public Step honeyHuntStep(StepBuilderFactory stepBuilderFactory,
            ItemReader<Honey> lightHoneyListReader,
            ItemProcessor<Honey, Honey> wideHoneyProcessor,
            ItemWriter<Honey> printHoneyWriter
            ) {
        return stepBuilderFactory
                .get("honeyHuntStep")
                .<Honey, Honey>chunk(10)
                .reader(lightHoneyListReader)
                .processor(wideHoneyProcessor)
                .writer(printHoneyWriter)
                .build();
    }
    
    @Bean
    public Step honeyMoonStep(StepBuilderFactory stepBuilderFactory,
            ItemReader<Honey> heavyHoneyListReader,
            ItemProcessor<Honey, Honey> narrowHoneyProcessor,
            ItemWriter<Honey> printHoneyWriter
            ) {
        return stepBuilderFactory
                .get("honeyMoonStep")
                .<Honey, Honey>chunk(10)
                .reader(heavyHoneyListReader)
                .processor(narrowHoneyProcessor)
                .writer(printHoneyWriter)
                .build();
    }

}
