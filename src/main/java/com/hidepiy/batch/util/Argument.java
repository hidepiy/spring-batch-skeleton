package com.hidepiy.batch.util;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

/**
 * Util of argument.
 * @author hideharu.hatayama
 *
 */
public class Argument {
    public static final String KEY_PREFIX="-";
    
    /**
     * get property map from args.
     * @param args
     * @return JobParameters
     * @throws IllegalArgumentException
     */
    public static JobParameters getJobParameters(String[] args) throws IllegalArgumentException {
        if (ArrayUtils.isEmpty(args)) { return new JobParameters(); }
        if (!isValidProperty(args)) {
            throw new IllegalArgumentException(
                    "Check the argumets!!! args: " + ArrayUtils.toString(args));
        }
        return createJobParameter(args);
    }

    /**
     * check the args are valid or not.
     * @param args
     * @return boolean result
     */
    private static boolean isValidProperty(String[] args) {
        if (ArrayUtils.isEmpty(args)) { return true; }
        if (args.length % 2 != 0) { return false; }
        for (int i = 0, length = args.length; i < length; i += 2) {
            if (!StringUtils.startsWith(args[i], KEY_PREFIX)) { return false; }
        }
        return true;
    }

    /**
     * create JobParameters from valid args.
     * @param validArgs
     * @return JobParameters
     */
    private static JobParameters createJobParameter(String[] validArgs) {
        JobParametersBuilder propertiesBuilder = new JobParametersBuilder();
        
        for (int i = 0, length = validArgs.length; i < length; i += 2) {
            propertiesBuilder.addString(StringUtils.removeStart(validArgs[i], KEY_PREFIX), validArgs[i + 1]);
        }
        return propertiesBuilder.toJobParameters();
    }
}
