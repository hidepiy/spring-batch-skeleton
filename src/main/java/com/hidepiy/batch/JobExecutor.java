package com.hidepiy.batch;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.hidepiy.batch.util.Argument;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class JobExecutor {
    public static void main(String...args) {
//        System.exit(SpringApplication.exit(SpringApplication.run(JobExecutor.class, args)));
        args = new String[]{"-batch_name", "HoneyHuntJob"};
        log.info("executeJob with args: " + ArrayUtils.toString(args));
        try (ConfigurableApplicationContext context = SpringApplication.run(JobExecutor.class, args)) {
            JobLauncher jobLauncher = context.getBean("jobLauncher", JobLauncher.class);
            JobParameters jobParameters = Argument.getJobParameters(args);
            String jobName = WordUtils.uncapitalize(jobParameters.getString("batch_name"));
            Job job = context.getBean(jobName, Job.class);
            JobExecution jobExecution = jobLauncher.run(job, jobParameters);
        } catch (Throwable e) {
            String message = 
                    "args: " + ArrayUtils.toString(args) + "\n"
                    + "Unknown error occured!!!." + "\n"
                    + ExceptionUtils.getStackTrace(e);
            log.error(message);
        }
    }

}
